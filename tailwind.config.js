/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{html,js}"
    ],
    theme: {
        extend: {
            fontFamily: {
            'sans' : ['IRANsans']
            },
            colors: {
                info: {
                    500: 'rgb(38, 165, 154)',
                }
            }
        },
    },
    plugins: [],
}

